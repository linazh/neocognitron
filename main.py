class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __radd__(self, other):
        return Point(self.x + other.x, self.y + other.y)

class Cell:
    def __init__(self, point = Point(), radius = 0):
        # connection area
        self.center = point
        self.radius = radius
        self.U_value = 0 # excitatory component
        self.V_value = 0 # inhibitory component

    def calc_values(self, prev_layer, cur_index, all_links):
        return


class SimpleCell(Cell):
    def __init__(self, point = Point(), radius = 0):
        super().__init__(point, radius)
        self.param_R = 1.0

    def calc_values(self, prev_layer, cur_index, all_links):
        links_A = all_links[0]
        links_B = all_links[1]
        links_C = all_links[2]

        K = len(prev_layer)
        summ_A = 0
        summ_C = 0

        for k in range(K):
            prev_plane = prev_layer[k]
            start_x = max(0, self.center.x - self.radius)
            start_y = max(0, self.center.y - self.radius)
            end_x = min(prev_plane.length, start_x + 2 * self.radius + 1)
            end_y = min(prev_plane.length, start_y + 2 * self.radius + 1)

            cell_matrix = prev_plane.plane
            for i in range(start_x, end_x):
                for j in range(start_y, end_y):
                    summ_A += links_A[cur_index][k][i - start_x][j - start_y] * cell_matrix[i][j].U_value
                    summ_C += links_C[i - start_x][j - start_y] * (cell_matrix[i][j].U_value ** 2)

            self.V_value = summ_C ** 0.5
            E_res = summ_A
            I_res = 2 * self.param_R / (1 + self.param_R) * links_B[cur_index] * self.V_value

            #print("E  ", E_res, "I  ", I_res, 'res = ', self.param_R * max(0, (1 + E_res) / (1 + I_res) - 1))

            self.U_value = self.param_R * max(0, (1 + E_res) / (1 + I_res) - 1)


class ComplexCell(Cell):
    def __init__(self, point = Point(), radius = 0):
        super().__init__(point, radius)
        self.alpha = 0.4

    def calc_values(self, prev_layer, cur_index, all_links):
        K = len(prev_layer)
        links_D = all_links[3] # A = 0, B = 1, C = 2, D = 3
        summ = 0
        summ_cur = 0
        for k in range(K):
            prev_plane = prev_layer[k]
            start_x = max(0, self.center.x - self.radius)
            start_y = max(0, self.center.y - self.radius)
            end_x = min(prev_plane.length, start_x + 2 * self.radius + 1)
            end_y = min(prev_plane.length, start_y + 2 * self.radius + 1)

            cell_matrix = prev_plane.plane
            for i in range(start_x, end_x):
                for j in range(start_y, end_y):
                    summ += links_D[i - start_x][j - start_y] * cell_matrix[i][j].U_value
                    if k == cur_index:
                        summ_cur += links_D[i][j] * cell_matrix[i][j].U_value
        self.V_value = 1 / K * summ

        X = (1 + summ_cur) / (1 + self.V_value) - 1

        #print(summ_cur, self.V_value, "XXXXX   ", X)

        self.U_value = max(0, X / (self.alpha + X))


class CellPlane:
    def __init__(self, cell_type = "Input", length = 1, point = Point(), radius = 0, index = 0):
        self.type = cell_type
        self.length = length
        self.index = index
        self.matrix_U = [[0] * length for i in range(length)]
        self.matrix_V = [[0] * length for i in range(length)]

        if self.type == "Simple":
            self.plane = [[SimpleCell(point + Point(i, j), radius) for j in range(length)] for i in range(length)]
        elif self.type == "Complex":
            self.plane = [[ComplexCell(point + Point(i, j), radius) for j in range(length)] for i in range(length)]
        elif self.type == "Input":
            self.plane = [[Cell(point + Point(i, j), radius) for j in range(length)] for i in range(length)]

    def fill_input_data(self, data):
        for i in range(self.length):
            for j in range(self.length):
                self.plane[i][j].U_value = data[i][j]

    def calculate_cell_values(self, prev_layer, all_links):
        if self.type == "Input":
            return

        for i in range(self.length):
            for j in range(self.length):
                self.plane[i][j].calc_values(prev_layer, self.index, all_links)

        self.recalc_matrix()

    def recalc_matrix(self):
        for i in range(self.length):
            for j in range(self.length):
                self.matrix_U[i][j] = self.plane[i][j].U_value
                self.matrix_V[i][j] = self.plane[i][j].V_value


class Module:
    def __init__(self, prev_layer, planes_num = 1, length_simple = 1, length_complex = 1,
                 point_simple = Point(), point_complex = Point(), radius_simple = 0, radius_complex = 0):
        self.size = planes_num
        self.prev_layer = prev_layer
        self.simple_layer = [CellPlane("Simple", length_simple, point_simple, radius_simple, i) for i in range(planes_num)]
        self.complex_layer = [CellPlane("Complex", length_complex, point_complex, radius_complex, i) for i in range(planes_num)]

        diameter_simple = 2 * radius_simple + 1
        diameter_complex = min(2 * radius_complex + 1, length_simple)
        prev_planes_num = len(prev_layer)

        self.param_Q = 1.0

        self.links_A = [[[[randint(1, 100) / 100 for i in range(diameter_simple)] for j in range(diameter_simple)]
                         for ii in range(prev_planes_num)] for k in range(self.size)]

        self.start_A = [[[[self.links_A[k][ii][j][i] for i in range(diameter_simple)] for j in range(diameter_simple)]
                         for ii in range(prev_planes_num)] for k in range(self.size)]

        self.links_B = [0] * planes_num

        self.links_C = [[0] * diameter_simple for i in range(diameter_simple)]
        self.links_D = [[0] * diameter_complex for i in range(diameter_complex)]

        self.all_links = [self.links_A, self.links_B, self.links_C, self.links_D]

        self.define_links(2)

        self.calculate()


    def define_links(self, value = 0, shape = 'square'):
        if (value == 0):
            diameter_C = len(self.links_C)
            diameter_D = len(self.links_D)

            valC = 1 / self.size / (diameter_C ** 2)
            valD = 1 / self.size / (diameter_D ** 2)
            for i in range(diameter_C):
                for j in range(diameter_C):
                    self.links_C[i][j] = valC

            for i in range(diameter_D):
                for j in range(diameter_D):
                    self.links_D[i][j] = valD

        elif (value == 1):
            finD = open("3x3_D.txt", "r")
            self.links_C = [[1 / 2 / 9]]

            for i in range(3):
                self.links_D[i] = list(map(float, finD.readline().split()))

        elif (value == 2):
            #shape in ["square", "romb", "cross"]
            fin = open(shape + ".txt")

            diameter_C = len(self.links_C)
            diameter_D = len(self.links_D)

            for i in range(diameter_C):
                for j in range(diameter_C):
                    self.links_C[i][j] = 1 / self.size / (diameter_C ** 2)

            for i in range(diameter_D):
                self.links_D[i] = list(map(float, fin.readline().split()))



    def calculate(self, study = False):
        if study:
            self.study_coeff()

        for i in range(self.size):
            self.simple_layer[i].calculate_cell_values(self.prev_layer, self.all_links)

        for i in range(self.size):
            self.complex_layer[i].calculate_cell_values(self.simple_layer, self.all_links)

    def study_coeff(self, study_mode = 0):
        if study_mode == 0:
            num_columns = 4
            length = self.simple_layer[0].length
            numRows = [0, length // 2,
                       0, length // 2,
                       length // 2, length,
                       length // 2, length]
            numCols = [0, length // 2,
                       length // 2, length,
                       0, length // 2,
                       length // 2, length]

            maximum = [-1] * num_columns
            planes = [0] * num_columns
            cell_index = [Point(0, 0) for i in range(num_columns)]

            num_S_planes = self.size
            representative = [-1] * num_S_planes # number in 'maximum' of the best cell in the plane

            for c in range(num_columns):
                for k in range(num_S_planes):
                    for i in range(numRows[2 * c], numRows[2 * c + 1]):
                        for j in range(numCols[2 * c], numCols[2 * c + 1]):
                            cur_plane = self.simple_layer[k]
                            cell_matrix = cur_plane.plane
                            value = cell_matrix[i][j].U_value
                            if value > maximum[c]:
                                maximum[c] = value
                                planes[c] = k
                                cell_index[c] = Point(i, j)
                prev_best = representative[planes[c]]
                if prev_best == -1 or maximum[prev_best] < maximum[c]:
                    representative[planes[c]] = c

            for i in range(num_S_planes):
                if representative[i] == -1:
                    continue
                best_index = representative[i]
                best_plane = self.simple_layer[planes[best_index]]
                cell_point = cell_index[best_index]
                best_cell = best_plane.plane[cell_point.x][cell_point.y]

                self.recalc_coeff(best_cell, planes[best_index])

    def recalc_coeff(self, repr_cell, plane_index):
        K = len(self.prev_layer)
        for k in range(K):
            prev_plane = self.prev_layer[k]
            start_x = max(0, repr_cell.center.x - repr_cell.radius)
            start_y = max(0, repr_cell.center.y - repr_cell.radius)
            end_x = min(prev_plane.length, start_x + 2 * repr_cell.radius + 1)
            end_y = min(prev_plane.length, start_y + 2 * repr_cell.radius + 1)

            par_Q = self.param_Q

            cell_matrix = prev_plane.plane
            for i in range(start_x, end_x):
                for j in range(start_y, end_y):
                    cur_C = self.links_C[i - start_x][j - start_y]

                    self.links_A[plane_index][k][i - start_x][j - start_y] += par_Q * cur_C * cell_matrix[i][j].U_value

            self.links_B[plane_index] += (par_Q / 2) * repr_cell.V_value


class NeocognitronSimple:
    def __init__(self, modules_num = 1, input_size = 28):
        self.input_U0 = [CellPlane("Input", input_size)]
        radius_S = 1
        radius_C = (input_size - 2) // 2
        self.module = Module(self.input_U0, 2, input_size - 2, 1,
                             Point(1, 1), Point(input_size // 2 - 1, input_size // 2 - 1), radius_S, radius_C)

    def self_organize(self, input_data):
        if type(input_data) == str:
            fin_num = open(input_data, "r")
            example_num = int(fin_num.readline())

            for i in range(example_num):
                index = int(fin_num.readline())
                #data = read_digit(index)  #from MNIST
                #data = small_data(index)
                data = read_10x10(index)
                self.input_U0[0].fill_input_data(data)
                self.module.calculate(True) # study == True

        if type(input_data) == list: # int of sys.argv[1:]
            example_num = input_data[0]
            patterns = input_data[1:]

            for i in range(len(patterns)):
                for j in range(example_num):
                    index = patterns[i]
                    data = read_10x10(index)
                    self.input_U0[0].fill_input_data(data)
                    self.module.calculate(True) # study == True

        """
        print(i + 1, index)
        print(self.module.complex_layer[0].plane[0][0].U_value)
        print(self.module.complex_layer[1].plane[0][0].U_value)
        print()
        #"""

    def request(self, pattern_index):
        data = read_10x10(pattern_index)

        self.input_U0[0].fill_input_data(data)
        self.module.calculate() # study == False

        C_plane_0 = self.module.complex_layer[0].plane[0][0].U_value
        C_plane_1 = self.module.complex_layer[1].plane[0][0].U_value

        print("Request", pattern_index)
        print(C_plane_0)
        print(C_plane_1)
        print()

        if C_plane_0 > C_plane_1:
            return 0
        if C_plane_0 < C_plane_1:
            return 1
        return -1


from struct import unpack

def read_digit(index):
    file_name = "train-images.idx3-ubyte"
    fin = open(file_name, 'rb')

    line = [fin.read(4) for i in range(4)]
    magicNum = unpack('>i', line[0])[0]
    numImages = unpack('>i', line[1])[0]
    numRows = unpack('>i', line[2])[0]
    numCols = unpack('>i', line[3])[0]

    start = fin.tell()
    pos = index - 1 # digit number
    fin.seek(numRows * numCols * pos + start)
    data = [[0] * numCols for i in range(numRows)]

    for i in range(numRows):
        for j in range(numCols):
            binCol = fin.read(1)
            color = int.from_bytes(binCol, byteorder = 'big')
            data[i][j] = color

    fin.close()

    return data

def small_data(index):
    if index == 0: #
        data = [[1, 0, 1],
                [0, 1, 0],
                [0, 0, 0]]
    elif index == 1:
        data = [[0, 0, 0],
                [1, 1, 1],
                [0, 0, 0]]
    return data

def read_10x10(index):
    fin = open("figure_" + str(index) + ".txt", 'r')

    data = [[0] * 10 for i in range(10)]

    for i in range(10):
        data[i] = list(map(int, fin.readline().split()))

    fin.close()
    return data


from random import randint
import sys


structure = NeocognitronSimple(1, 10)
#input_file = "nums_0.txt"
#structure.self_organize(input_file)

input_data = list(map(int, sys.argv[1:]))
structure.self_organize(input_data)


patterns_num = len(input_data[1:])
requests = input_data[1:]
result = [0] * patterns_num
for i in range(patterns_num):
    result[i] = structure.request(requests[i])

"""
if result[0] != result[1] and min(result) != -1:
    fout = open("start_A_links.txt", 'a')
    line_A = structure.module.start_A

    print(requests,'\n', line_A, file = fout, sep = '')
    fout.close()
"""