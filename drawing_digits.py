from struct import unpack
import turtle

def draw_pixel(col):
    turtle.pencolor((col, col, col))
    turtle.fillcolor((col, col, col))
    turtle.pendown()
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(1)
        turtle.left(90)
    turtle.end_fill()
    turtle.penup()

file_name = "train-images.idx3-ubyte"
fin = open(file_name, 'rb')

line = [fin.read(4) for i in range(4)]
magicNum = unpack('>i', line[0])[0]
numImages = unpack('>i', line[1])[0]
numRows = unpack('>i', line[2])[0]
numCols = unpack('>i', line[3])[0]

start = fin.tell()
pos = int(input()) # digit number
pos -= 1
fin.seek(numRows * numCols * pos + start)

screen = turtle.Screen()
screen.screensize(numRows, numCols)
screen.setworldcoordinates(0, numRows, numCols, 0)

turtle.title("# " + str(pos + 1) + "  Image from MNIST")

turtle.home()
turtle.colormode(255)
turtle.speed(0)
turtle.tracer(False)

for i in range(numRows):
    for j in range(numCols):
        binCol = fin.read(1)
        color = int.from_bytes(binCol, byteorder = 'big')
        turtle.penup()
        turtle.goto(j, i)
        draw_pixel(255 - color)      
        
turtle.mainloop()
        